<?php
namespace PUGX\BookBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class BookRepository extends EntityRepository
{
    public function findAllWithAuthors()
    {
        $query = $this->createQueryBuilder('b')
            ->select('b', 'a')
            ->innerJoin('b.author', 'a')
            ->orderBy('b.title',  'ASC')
            ->getQuery()
        ;

        return $query->getResult();
    }
}
