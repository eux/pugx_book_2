<?php

namespace PUGX\BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\BookBundle\Entity\Author;

/**
 *
 * @ORM\Entity(repositoryClass="PUGX\BookBundle\Repository\BookRepository")
 * @ORM\Table(name="book")
 */
class Book
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="PUGX\BookBundle\Entity\Author", inversedBy="books")
     **/
    protected $author;

    /**
     * @ORM\Column(type="string", name="title", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="date", name="publication_date")
     */
    protected $publicationDate;

    /**
     * @param $title
     * @param $author
     * @param \DateTime $publishDate
     */
    public function __construct($title = null, $author = null, \DateTime $publishDate = null)
    {
        if (!is_null($title)) {
            $this->setTitle($title);
        }
        if (!is_null($author)) {
            $this->setAuthor($author);
        }
        if (!is_null($publishDate)) {
            $this->setPublicationDate($publishDate);
        }
    }

    /**
     * @param Author $author
     */
    public function setAuthor(Author $author)
    {
        $this->author = $author;
    }

    /**
     * @return Author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param \DateTime $publishDate
     */
    public function setPublicationDate(\DateTime $publishDate)
    {
        $this->publicationDate = $publishDate;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
