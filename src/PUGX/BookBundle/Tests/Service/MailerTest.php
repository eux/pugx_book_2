<?php

namespace PUGX\BookBundle\Tests\Service;

use PUGX\BookBundle\Service\Mailer;

class MailerTest extends \PHPUnit_Framework_TestCase {
    protected $swiftMailer;
    protected $book;
    protected $author;
    protected $mailer;

    public function setUp()
    {
        $this->swiftMailer = $this->getMockBuilder('\Swift_Mailer')
            ->disableOriginalConstructor()
            ->getMock();

        $this->book = $this->getMockBuilder('PUGX\BookBundle\Entity\Book')
            ->disableOriginalConstructor()
            ->getMock();

        $this->author = $this->getMockBuilder('PUGX\BookBundle\Entity\Author')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mailer = new Mailer($this->swiftMailer);
    }

    public function testSendNewBookNotification()
    {
        $this->book->expects($this->exactly(1))
            ->method('getAuthor')
            ->will($this->returnValue($this->author));

        $this->book->expects($this->exactly(2))
            ->method('getTitle')
            ->will($this->returnValue('Gödel, Escher, Bach: An Eternal Golden Braid'));

        $this->book->expects($this->exactly(1))
            ->method('getPublicationDate')
            ->will($this->returnValue(new \DateTime('1979-05-31')));

        $this->author->expects($this->exactly(1))
            ->method('__toString')
            ->will($this->returnValue('Douglas Hofstadter'));

        $this->swiftMailer->expects($this->exactly(1))
            ->method('send')
            ->with($this->isInstanceOf('\Swift_Message'));

        $this->swiftMailer->expects($this->exactly(1))
            ->method('send')
            ->with($this->isInstanceOf('\Swift_Message'));


        $this->mailer->sendNewBookNotification($this->book, 'foo@bar.com','bar@foo.com');
    }
}